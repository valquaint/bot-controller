var http = require('http').createServer(handler);
var fs = require('fs');
var io = require('socket.io')(http);
const { exec } = require('child_process');
http.listen(14987);
function handler(req, res) {
    fs.readFile(__dirname + '/index.html', function (err, data) {
        if (err) {
            res.writeHead(404, { 'Content-Type': 'text.html' });
            return res.end("404 Not Found");
        }
        res.writeHead(200, { 'Content-Type': 'text.html' });
        res.write(data);
        return res.end();
    });
}
io.sockets.on('connection', function (socket) {
    var lightvalue = 0;
    socket.on('Reboot', function (data) {
        pwd = data;
        if (pwd == "Blazed2019") {
            console.log("Hadasa system rebooting. Command: Reboot");
            exec('sudo reboot', (err, stdout, stderr) => {
                if (err) {
                    console.log("Error executing command.");
                    return;
                }
                console.log(`stdout: ${stdout}`);
                console.log(`stderr: ${stderr}`);
            });
        }
        else {
            console.log("Invalid password: " + pwd);
        }
    });
});
//# sourceMappingURL=index.js.map