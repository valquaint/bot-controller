var http = require('http').createServer(handler);
var fs = require('fs');
var io = require('socket.io')(http);
const { exec } = require('child_process');
http.listen(14987); //Listen on our preferred port

function handler(req, res) {
	fs.readFile(__dirname + '/index.html', function(err, data) {
		if(err){
			res.writeHead(404, {'Content-Type': 'text.html'});
			return res.end("404 Not Found");
		}
		res.writeHead(200, {'Content-Type': 'text.html'});
		res.write(data);
		return res.end();
	});
}


io.sockets.on('connection', function (socket) {// WebSocket Connection
  var lightvalue = 0; //static variable for current status
  socket.on('Reboot', function(data) { //get light switch status from client
    pwd = data;
    if (pwd=="Blazed2019") {
      console.log("Hadasa system rebooting. Command: Reboot"); 
	  exec('sudo reboot', (err, stdout, stderr) => {
		  if (err) {
			console.log("Error executing command.");// node couldn't execute the command
			return;
		  }

		  // the *entire* stdout and stderr (buffered)
		  console.log(`stdout: ${stdout}`);
		  console.log(`stderr: ${stderr}`);
		});
    }else{
		console.log("Invalid password: "+pwd);
	}
  });
});